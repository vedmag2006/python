import csv
import urllib.request
import numpy as np
from bs4 import BeautifulSoup

BASE_URL = 'https://weblancer.net/jobs/'
def get_html(url):
    response = urllib.request.urlopen(url)
    return response.read()

def get_page_count(html):
    soup = BeautifulSoup(html)
    paggination = soup.find('div', class_='col-1 col-sm-2 text-right')
    for link in (paggination.find_all('a')):
        return int(link.get('href').split("=")[1])


def parse(html):
    soup = BeautifulSoup(html)

    projects = {}

    for table in soup.find('div', class_='cols_table divided_rows'):
        rows = table.find_all('div', class_="title")
        discrip = table.find_all('div', class_="text_field text-inline")
        pr =  table.find_all('div', class_="float-right float-sm-none title amount indent-xs-b0")
        numb = table.find_all('div', class_="float-left float-sm-none text_field")

        projects.update({
            'title': rows[0].a.text,
            'discription': discrip[0].text,
            'price': pr[0].text,
            'number': numb[0].text.strip().split()[0]
        })
    return projects

    #for project in projects:
    #    print(project)

def save(projects, path):
    print(projects)
    with open(path, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(('Проект', 'Категории', 'Цена', 'Заявки'))

        for project in projects:
            print(project['title'], project['discription'], project['price'], project['number'])
            
            writer.writerow((project['title'], project['discription'], project['price'], project['number']))

def main():
    page_count = get_page_count(get_html(BASE_URL))
    
    print("Всего страниц:", page_count)

    projects = []

    for page in range(1, 5):
        print('Парсинг %d%%' % (page / page_count * 100))
        projects.append(parse(get_html(BASE_URL + '?page=%d' % page)))
    
    save(projects, 'projects.csv')

if __name__ == '__main__':
    main()