# -*- coding: UTF-8 -*-
import sys
import hvac
import yaml
import host_config

url = sys.argv[1]
token = sys.argv[2]

mount_point = "ansible"
stands_point = "secret"

client = hvac.Client(url=url)
client.token = token

##################################################################################
# добавление файла с данными
with open("terra-yaml.yml", 'r') as file:
    a_dict = yaml.full_load(file)
    # Выборка всех хостов и создание единого списка из всех этераций for для pprb
    all_nodes_pprb = {}
    all_nodes_efs = {}
    for key in a_dict['stand_definition_pprb']['children']:
        nodes_pprb = dict((k,"") for k in (list(a_dict['stand_definition_pprb']['children'][key])))
        all_nodes_pprb = {**all_nodes_pprb, **nodes_pprb}
# Добавление данных в vault для pprb
    client.secrets.kv.v1.create_or_update_secret(
        mount_point=mount_point,
        path='stands/pprb/nodes',
        secret=all_nodes_pprb,
    )
    # Выборка всех хостов и создание единого списка из всех этераций for для efs
    for key in a_dict['stand_definition_efs']['children']:
        nodes_efs = dict((k,"") for k in (list(a_dict['stand_definition_efs']['children'][key])))
        all_nodes_efs = {**all_nodes_efs, **nodes_efs}
    # Добавление данных в vault для efs
    client.secrets.kv.v1.create_or_update_secret(
        mount_point=mount_point,
        path='stands/efs/nodes',
        secret=all_nodes_efs,
    )
##################################################################################
# создаем конфиг хоста для всех хостов pprb
for host in all_nodes_pprb:
    client.secrets.kv.v1.create_or_update_secret(
        mount_point=mount_point,
        path=("stands/pprb/nodes/{0}/config".format(host)),
        secret=host_config.config,
        )
# создаем конфиг хоста для всех хостов efs
for host in all_nodes_efs:
    client.secrets.kv.v1.create_or_update_secret(
        mount_point=mount_point,
        path=("stands/efs/nodes/{0}/config".format(host)),
        secret=host_config.config,
        )
##################################################################################
# создаем common для стендов
# pprb
vars_pprb = a_dict['stand_definition_pprb']['vars']['stand']
n_pprb = yaml.dump(vars_pprb)
common_pprb = {'vars' : n_pprb}
client.secrets.kv.v1.create_or_update_secret(
    mount_point=mount_point,
    path='stands/pprb/common',
    secret=common_pprb,
)
# efs
vars_efs = a_dict['stand_definition_efs']['vars']['stand']
n_efs = yaml.dump(vars_efs)
common_efs = {'vars' : n_efs}
client.secrets.kv.v1.create_or_update_secret(
    mount_point=mount_point,
    path='stands/efs/common',
    secret=common_efs,
)
##################################################################################
# запихиваем роли
# создаем roles для pprb
roles_pprb = a_dict['stand_definition_pprb']['children']

# создаем roles для efs
roles_efs = a_dict['stand_definition_efs']['children']

#создаем функцию для перебора по all_nodes_efs, roles_efs, all_nodes_pprb, roles_pprb
def true_roles(all_nodes, roles):
    result = {}
    for k1, v1 in all_nodes.items():
        for k2, v2 in roles.items():
            if k1 in v2.keys():
                if k1 not in result.keys():
                    result[k1] = {}
                result[k1].update({k2: ""})
    return result

#создаем функцию для перебора по all_nodes_efs, all_nodes_pprb, true_roles, efs, pprb
def true_path(all_nodes, result_roles, stand):
    for host in all_nodes:
        client.secrets.kv.v1.create_or_update_secret(
            mount_point=mount_point,
            path=(f"stands/{stand}/nodes/{host}/roles"),
            secret=result_roles[host],
            )

true_path(all_nodes_efs, true_roles(all_nodes_efs, roles_efs), "efs")
true_path(all_nodes_pprb, true_roles(all_nodes_pprb, roles_pprb), "pprb")