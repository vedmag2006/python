# стандартная конфигурация для хостов создаваемых с помощью Terraform

config = {
    'boot_volume_size' : '30',
    'flavor_id' : 'a1719f7f-ba19-47c2-aca6-094975d70968',
    'flavor_name' : 'm1.tiny',
    'image_id' : '8fca3bb9-bf38-497e-ac13-ccc5683ceaed',
    'image_name' : 'iac_linux_rhel_7.7_x86_64_ceph',
    'network_names' : 'net-172.27.0.0-24',
    'security_groups' : 'default,',
}
